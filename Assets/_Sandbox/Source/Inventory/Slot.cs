﻿namespace Sandbox.Inventory
{
    public class Slot
    {
        private Item _item;
        public bool Occupied => _item != null;

        public Slot(Item item)
        {
            _item = item;
        }

        public Slot()
        {
        }

        public void Add(Item item)
        {
            if (Occupied)
            {
                throw new System.InvalidOperationException("Slot is already occupied!");
            }
            
            _item = item;
        }
        
        public Item Remove()
        {
            if (Occupied == false)
            {
                throw new System.InvalidOperationException("Slot is not occupied!");
            }
            
            Item oldItem = _item;
            _item = null;

            return oldItem;
        }
    }
}