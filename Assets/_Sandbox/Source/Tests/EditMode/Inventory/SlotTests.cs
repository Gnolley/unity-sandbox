using System;
using NUnit.Framework;
using Sandbox.Inventory;
using UnityEditor;

public class SlotTests
{
    [Test]
    public void Occupancy_NewItem_IsOccupied()
    {
        var testItem = new Item("100");
        var testSlot = new Slot(testItem);
        
        Assert.That(testSlot.Occupied, Is.True, "Slot was not occupied!");
    }
    
    [Test]
    public void Occupancy_NoItem_IsNotOccupied()
    {
        var testSlot = new Slot();
        Assert.That(testSlot.Occupied, Is.False, "Slot was occupied, but nothing was given to occupy it!");
    }

    [Test]
    public void AddItem_NewItem_SlotOccupied()
    {
        var testItem = new Item("100");
        var testSlot = new Slot();

        testSlot.Add(testItem);
        
        Assert.That(testSlot.Occupied, Is.True, "Slot was not occupied!");
    }
    
    [Test]
    public void RemoveItem_NewItem_SlotNotOccupied()
    {
        var testItem = new Item("100");
        var testSlot = new Slot(testItem);

        testSlot.Remove();
        
        Assert.That(testSlot.Occupied, Is.False, "Slot was occupied!");
    }

    [Test]
    public void RemoveItem_Empty_ThrowsInvalidOperation()
    {
        var testSlot = new Slot();
        Assert.Throws<InvalidOperationException>(() => testSlot.Remove());
    }
    
    [Test]
    public void AddItem_AlreadyOccupied_ThrowsInvalidOperation()
    {
        var testItem1 = new Item("101");
        var testItem2 = new Item("102");
        
        var testSlot = new Slot(testItem1);
        Assert.Throws<InvalidOperationException>(() => testSlot.Add(testItem2));
    }
}
