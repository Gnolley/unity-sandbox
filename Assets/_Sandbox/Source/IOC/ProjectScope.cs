using VContainer;
using VContainer.Unity;

namespace Sandbox.IOC
{
    public class ProjectScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            base.Configure(builder);
        }
    }
}
